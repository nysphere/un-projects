Enables soon-to-be-retiree's to smoothly complete their retirement process. Gives overall completion progress information along with completion progress for each section. 

Admins can see each individuals progress. 

System generates reminder emails for those who are soon to be retired but have not completed the process yet.

No frontend framework is used. After the initial page load, all the data transactions are done by using ajax calls to backend api.

