$(()=>{
	
	var $logList = $('#logList');
	function renderLog(copy, mode){
		$logList.append($('<div>').addClass('alert alert-'+(mode||'default')).html(copy));
	}

	$('#inputFile').on('change', function(e){
		getTextFileContents($(this)[0]);
	});

	function getTextFileContents(filePath) {
		var fileContents = "";
		if(filePath.files && filePath.files[0]) {  
			var reader = new FileReader();
			reader.onload = function (e) {
				fileContents = e.target.result;
				renderLog('File read','success');
				afterReading(fileContents);
			};
			reader.readAsText(filePath.files[0]);
		}else {
			return false;
		}       
		return true;
	}

	function afterReading(fileContents){
		
		var tsvObjects;

		var requiredHeaderList = ['Category', 'Grade', 'Gender', 'Activity', 'Course Name', 'Course Category', 'Delivery Method', 'Enrollment ID', 'Enrollment Status', 'Activity Start Date', 'Completion Date', 'Learning Environment']; 
		var headerMap = {
			'Category':{name:'category', dataType:"String"},
			'Grade':{name:'grade', dataType:"String"},
			'Gender':{name:'gender', dataType:"String"},
			'Activity':{name:'activity', dataType:"String"},
			'Course Name':{name:'courseName', dataType:"String"},
			'Course Category':{name:'courseCategory', dataType:"String"},
			'Delivery Method':{name:'courseDeliveryMethod', dataType:"String"},
			'Enrollment ID':{name:'enrollmentId', dataType:"Number"},
			'Enrollment Status':{name:'enrollmentStatus', dataType:"String"},
			'Activity Start Date':{name:'startDate', dataType:"String"},
			'Completion Date':{name:'completionDate', dataType:"String"},
			'Learning Environment':{name:'learningEnvironment', dataType:"String"}
		};

		try {
			var rows = CSV.parse(fileContents, /[\r\n]/,',',true, requiredHeaderList, headerMap);
			if (rows){
				renderLog('CSV Data parsed','success');
				
				$('#groupImportToDatabase').show();
				$('#buttonImportToDatabase').prop('disabled', false);
				
				$('#buttonImportToDatabase').on('click', function(){
					$('#buttonImportToDatabase').prop('disabled', true);
					importRows(rows);								
				});

			}
		} catch (e) {
			renderLog('Could not convert CSV to JSON: ' + e.message,'danger');
		}

	}

	function constructDataTable(rows){
		if (rows){
			var $dataTable = $('#dataTable');
			$dataTable.empty();
			rows.forEach(function(row){
				var $tr = $('<tr>');
				$tr.append($('<td>').html(row.indexNumber));
				$tr.append($('<td>').html(row.inspiraId));
				$tr.append($('<td>').html(row.firstName));
				$tr.append($('<td>').html(row.lastName));
				$tr.append($('<td>').html(row.email));
				$tr.append($('<td>').html(row.grade));
				$tr.append($('<td>').html(row.activity));
				$tr.append($('<td>').html(row.courseName));
				$tr.append($('<td>').html(row.courseDeliveryMethod));
				$dataTable.append($tr);

			})
			renderLog('Table constructed','success');
		}
	}

	// Limited concurrency, partial array upload of the flat CSV data
	// Back-end consumes the queue collection and adds each row in normalized manner
	function importRows(rows){
		var numberOfRunning = 0;
		var numberOfConcurrent = 10;
		var numberOfRowsAtOnce = 20;
		var failedRows = [];
		var rowsOriginalLength = rows.length
		$('#numberOfRows').html(rowsOriginalLength);
		$('#timestampStarted').html(new Date());

		function ajaxCall(r_ows){
			$.ajax({
				type: "POST",
				url: '/api/1/Queue',
				dataType: "json",
				data: {
					rows: r_ows
				},
				success: function (json) {
					numberOfRunning--;
					$('#timestampLastProcessed').html(new Date());
					r_ows.forEach(function(row, index){
						if (json.failedIndices.indexOf(index)!==-1) {
							var uploadAttempt = row.uploadAttempt || 0;
							if (uploadAttempt<3){
								row.uploadAttempt = uploadAttempt + 1;
								rows.push(row);
							}else{
								failedRows.push(row);
							}
						}
					});
					attemptImport();
				},
				error: function (error) {
					console.log(error);
					numberOfRunning--;
					r_ows.forEach(function(row){
						var uploadAttempt = row.uploadAttempt || 0;
						if (uploadAttempt<3){
							row.uploadAttempt = uploadAttempt + 1;
							rows.push(row);
						}else{
							failedRows.push(row);
						}
					})
					attemptImport();
				}
			});
		}

		function attemptImport(){
			$('#numberOfProcessed').html(rowsOriginalLength - rows.length - numberOfRunning);
			$('#numberOfBeingProcessed').html(numberOfRunning);
			$('#numberOfRemaining').html(rows.length);
			setTimeout(function(){
				if (rows.length>0){
					
					while (rows.length>0 && numberOfRunning<numberOfConcurrent){
						numberOfRunning++;
						var _rows = [];
						for (var i=0; i<numberOfRowsAtOnce;i++){
							if (rows.length>0){
								_rows.push(rows.shift());
							}else{
								break;
							}
						}
						
						ajaxCall(_rows);
						
					}

				}else if (numberOfRunning==0){
					if (failedRows.length>0){
						renderLog(failedRows.length + ' failed to upload','danger');
					}else{
						renderLog('Upload completed','success');
					}
				}
			},0);
		}
		attemptImport();
	}


});