A list of projects I built as a full-stack developer at UNHQ OHRM. For all projects:

The back-end is Node Express Mongodb

The front-end is Javascript jQuery with Ajax, and Bootstrap

Unfortunately, I cannot upload the code. Upon request, I can demonstrate each site (especially the CMS I built for the system admins) using a screenshare software.

I have built the Induction and the Preretirement sites with multi-language support. A new language can be added in 10 minutes, without needing to modify database structure. (A 'System Admin' needs to give a native speaker of the new language 'Content Admin' and 'Interface Admin' roles so that s/he can add entries for each content piece and each interface element)