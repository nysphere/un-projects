The actual implementation of Global Induction Platform at

[https://uninduction.parseapp.com/](https://uninduction.parseapp.com/).

I have been migrating the app from parse.com servers to UN internal servers now. The url will be induction.un.org soon. 

And another installation of the app with customizations at

[https://unwomeninduction.parseapp.com/](https://unwomeninduction.parseapp.com/)
